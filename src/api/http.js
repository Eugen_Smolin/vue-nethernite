import axios from 'axios'

const baseURL = 'https://registry.npmjs.org';

const http = axios.create({
	baseURL: baseURL,
	timeout: 10000,
});

export { http };
