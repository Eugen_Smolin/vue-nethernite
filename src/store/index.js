import Vue from 'vue';
import Vuex from 'vuex';

import packagesModule from './modules/packages';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    packagesModule
  },
});
