import { http } from '@/api/http';
import { API_ROUTES } from '@/api/api-routes';
import { httpStatus } from '@/utils/const';

export default {
  namespaced: true,
  state: {
    list: [],
    status: null,
    pagination: {
      page: 1,
      size: 10, // take
      from: 0, // by size/10
      total: 0,
    },
    params: {
      text: '',
    },
    dialog: false,
    showedItem: {},
  },
  getters: {
    list: (state) => state.list,
    status: (state) => state.status,
    pagination: (state) => state.pagination,
    dialog: (state) => state.dialog,
    showedItem: (state) => state.showedItem,
  },
  mutations: {
    SET_DATA: (state, list) => {
      const { objects, total } = list;
      state.list = objects.map((item) => item.package);
      state.pagination.total = Math.ceil(total / state.pagination.size);
    },
    SET_PAGINATION: (state, page) => {
      state.pagination.page = page;
      state.pagination.from = state.pagination.size * (page - 1);
    },
    SET_PARAMS: (state, params) => {
      state.params = { ...state.params, ...params };
    },
    SET_DIALOG: (state, payload) => {
      state.dialog = payload;
    },
    SET_SHOWED_ITEM: (state, item) => {
      state.showedItem = item;
    },
  },
  actions: {
    async getList({ commit, state }) {
      try {
        state.status = httpStatus.PENDING;
        const { text } = state.params;
        const { from, size } = state.pagination;
        const res = await http.get(API_ROUTES.packages.get, {
          params: { text, from, size }
        });
        commit('SET_DATA', res.data);
        state.status = httpStatus.SUCCESS;
      } catch (err) {
        console.log(err);
        state.status = httpStatus.FAIL;
      }
    },
  },
};
